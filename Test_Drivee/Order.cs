﻿using System.Drawing;

namespace Test_Drivee
{
    public class Order : IComparable
    {
        public int ID { get; set; }
        public PointF Origin { get; set; }
        public PointF Destination { get; set; }
        public float Price { get; set; }

        public Courier Courier { get; set; }
        public float Distance { get; set; }

        public Order(PointF A, PointF B, float price)
        {
            this.Courier = new Courier();
            this.Distance = .0f;
            this.Origin = A;
            this.Destination = B;
            this.Price = price;
        }

        private float CalcDistanceSegment()
        {
            float powX = MathF.Pow(Destination.X - Origin.X, 2);
            float powY = MathF.Pow(Destination.Y - Origin.Y, 2);
            float dist = MathF.Sqrt(powX + powY);
            return dist;
        }

        public int CompareTo(object? obj)
        {
            if (obj is Order order)
            {
                return CalcDistanceSegment().CompareTo(order.CalcDistanceSegment());
            }
            else
            {
                throw new ArgumentException("Некорректное значение параметра");
            }
        }
    }
}
