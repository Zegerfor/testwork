﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;
using System.Drawing;
using Test_Drivee;

string fileName = "C:\\Users\\xagon\\source\\repos\\Test_Drivee\\Test_Drivee\\ListOrder.json";
string jsonString = File.ReadAllText(fileName);
List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(jsonString);

fileName = "C:\\Users\\xagon\\source\\repos\\Test_Drivee\\Test_Drivee\\ListCourier.json";
jsonString = File.ReadAllText(fileName);
List<Courier> couriers = JsonConvert.DeserializeObject<List<Courier>>(jsonString);

if (orders == null || couriers == null)
{
    Console.WriteLine("Список заказов или курьеров пуст");
    return;
}

/* Сортируем список заказов по возрастанию в зависимости от длины от точки получения до точки доставки
 * с помощью реализации метода CompareTo интерфейса IComparable для класса Order*/
orders.Sort();

/* Лямбда-функция для поиска длины между двумя точками*/
var CalcDistanceSection = (PointF point1, PointF point2) =>
{
    float powX = MathF.Pow(point1.X - point2.X, 2);
    float powY = MathF.Pow(point1.Y - point2.Y, 2);
    float dist = MathF.Sqrt(powX + powY);
    return dist;
};


/* Для каждого заказа ищем ближайшего курьера
* Отправляем ближайшего курьера в конец списка курьеров.
* Последний в списке курьер не будет рассматриваться для следующих заказов */
for (int i = 0; i < orders.Count; i++)
{
    float min = CalcDistanceSection(orders[i].Origin, couriers[0].Location);
    int shortWayIndex = -1;
    if (couriers.Count - i > 0)
    {
        for (int j = 0; j < couriers.Count - i; j++)
        {
            float dist = CalcDistanceSection(orders[i].Origin, couriers[j].Location);

            if (dist <= min)
            {
                min = dist;
                shortWayIndex = j;
            }
            //Console.WriteLine($"{orders[i].ID} to {couriers[j].ID}  |   dist:{dist}");
        }
        Courier tmp = couriers[shortWayIndex];
        
        couriers.Remove(tmp);
        couriers.Add(tmp);

        orders[i].Distance = min;
        orders[i].Courier = tmp;
    }
}

IEnumerable<Order> result = orders.OrderBy(x => x.ID);

foreach (var c in result)
{
    int courId = c.Courier.ID;
    int orderId = c.ID;
    float distance = MathF.Round(c.Distance,2);
    float distanceSecion = MathF.Round(CalcDistanceSection(c.Origin, c.Destination));

    Console.WriteLine($"Order ID: {orderId}, Courier ID: {courId}, DistanceCourToOrigin: {distance}, DistanceCourToOriginToDestination: {distance + distanceSecion}");
}