﻿using System.Drawing;

namespace Test_Drivee
{
    public class Courier
    {
        public int ID { get; set; }
        public PointF Location { get; set; }

        public Courier()
        {
            ID = -1;
            Location = new PointF();
        }
        public Courier(PointF Location)
        {
            this.Location = Location;
        }
    }
}
